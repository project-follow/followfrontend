import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardViewComponent} from './views/dashboard-view/dashboard-view.component';

const routes: Routes = [
  {path: 'dashboard', component: DashboardViewComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
